<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (file_exists(dirname(__FILE__) . '/local.php')) {
  include('local.php');
} else { 
  // ** MySQL settings - You can get this info from your web host ** //
  /** The name of the database for WordPress */
  define('DB_NAME', 'db188091_24');

  /** MySQL database username */
  define('DB_USER', 'bbTest1337!');

  /** MySQL database password */
  define('DB_PASSWORD', 'bbTest1337!');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd**;/QmD$r-L`Q[tf},|-qZmB39u.<lo(to*p*:a{aX#:doHyG7!(5dQu-X:ui`F');
define('SECURE_AUTH_KEY',  'yuej=ysc#f2|L76e!?elDrLP@Pf{WH5F&5:X0plfv,w%Nx=R[cDy=M[8b{xo2g[Z');
define('LOGGED_IN_KEY',    'k}[A40Twu<#{uz]$ $HdLy.@eiYTj$jiq1Leck@1Kh}SB|RftjSW`z/XEQl<:w#d');
define('NONCE_KEY',        '_N({_:53L;Pg~R{S|Koy3t|HVV0qJNC8o2ec4)HQUW(k:*+/3fnPNVxH(lwkJ{(f');
define('AUTH_SALT',        '=zZAI:njb[=OSO~Ooz_eH*ANo!hc?4 aXX5*n(=,n$LFgo1oD-6<?#rpjZy|tTCT');
define('SECURE_AUTH_SALT', 'oZ2GsXr$*kZ=iTSsp2m$[8&PichUhsOZhC}wn7/{w>s?.7=2k4a0hY z:xD_/v{j');
define('LOGGED_IN_SALT',   'H~(b5*22;fxYaa3W:TIuBMUlwsy!xoT%dx/2E7r]MiX,$q0Ld8Y3iH@KU%D0Q~vD');
define('NONCE_SALT',       ')>+>76 lk~6:b_C;FBK}0zvN|>Ke7l74%M>+}(bFOl64Fk-o;%Z^#Z%;g#+,KB i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
