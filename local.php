<?php
/** DB Settings for the local installation */

/** The name of the database for WordPress */
define('DB_NAME', 'bbtest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

